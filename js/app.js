
$(function () {

  var data = {
    "intro": {
      "title": "Are you ready?",
      "details": "<p class=\"text-justify\">We are very thrilled to welcome you all to BA Seminar 2019 in Fukuoka, Japan on Aprill 22-26, 2019. Our BA Seminar website offers you the essential information you need to know before the trip. Now that you have registered for air tickets, please log in again to complete the full registration and find out more about the key happenings of the BA seminar.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut iaculis urna tortor, nec convallis metus rutrum vel. Sed aliquam, ligula a iaculis sodales, quam velit volutpat tortor, eu posuere elit urna in ex. Ut sed ante mollis, blandit ante vitae, sodales urna. Sed id diam tortor. Pellentesque at aliquet felis. Duis dignissim elementum augue bibendum congue. Aenean in lorem sed urna tincidunt interdum eleifend ullamcorper odio. Aenean tempus vel urna ut faucibus. Etiam auctor, elit quis tristique vehicula, ligula ipsum convallis elit, eget dignissim lectus turpis nec est. Mauris euismod dapibus pulvinar. Vivamus in efficitur tortor, at commodo arcu. Praesent molestie maximus pellentesque. Etiam nec mollis sem. Cras pharetra et justo et sollicitudin. Phasellus finibus leo dapibus, dapibus velit non, iaculis nulla. Sed egestas venenatis pulvinar.</p>"
    },
    "pricing": {
      "title": "Pricing",
      "details": "<p class=\"text-justify\">Etiam egestas convallis hendrerit. Fusce eget nunc ultrices, iaculis arcu ac, varius eros. Integer auctor sollicitudin nulla at congue. Pellentesque fermentum suscipit erat. Maecenas nec metus in velit ullamcorper porttitor. Nullam sapien metus, venenatis vel eros et, ultrices laoreet ipsum. Quisque a felis scelerisque, laoreet odio ac, porta arcu. Mauris at ligula finibus, malesuada metus id, pretium neque. Aliquam erat volutpat. Fusce vulputate odio diam, dignissim tristique arcu fringilla et. Quisque vel viverra libero. Phasellus ligula lectus, varius id ligula sit amet, rutrum pharetra augue. Morbi lobortis, diam eu pulvinar eleifend, felis ipsum luctus arcu, eu condimentum lorem purus non justo. Nulla vehicula lectus mauris, in rhoncus lorem tincidunt sit amet.</p>"
    },    
    "schedule": {
      "title": "Schedule",
      "details": "<p class=\"text-justify\">Quisque eget urna eros. Donec ante ex, varius in odio eget, mattis hendrerit ligula. Donec id felis vitae dui sollicitudin congue at vitae libero. Etiam turpis orci, auctor rhoncus mauris euismod, malesuada finibus velit. Nunc in imperdiet ex. Nullam vehicula elit sapien, quis commodo velit vulputate nec. Vivamus non condimentum tortor. Sed non tellus at neque consequat sagittis quis ac nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec diam velit, aliquet nec justo id, congue auctor nunc. Donec sit amet rutrum elit. Suspendisse consectetur pellentesque enim, pulvinar posuere nibh malesuada posuere.</p>"
    },
    "hotel": {
      "title": "Hotel",
      "details": "<p class=\"text-justify\">Vestibulum ac felis elementum, hendrerit neque nec, pretium magna. Morbi facilisis non diam et pellentesque. Proin interdum rhoncus ex, semper dapibus tortor convallis quis. Ut sit amet odio neque. Mauris id ante sit amet dolor efficitur scelerisque. Ut accumsan odio vitae neque bibendum ultrices. Praesent felis ligula, tincidunt id hendrerit sit amet, mattis et diam. Praesent vitae est ut ex elementum imperdiet. Proin a maximus urna. Maecenas ullamcorper volutpat facilisis. Vestibulum sapien velit, feugiat varius suscipit eu, finibus in diam. Phasellus a malesuada sapien, id viverra quam. Aenean aliquam leo porta, imperdiet metus sollicitudin, lobortis nisi. Vivamus viverra, quam ut gravida feugiat, augue arcu dignissim enim, eget posuere eros magna nec velit. Maecenas dignissim sodales mauris, in congue ipsum placerat sed. Curabitur eu tempus dui, vitae volutpat est.</p>"
    },
    "teambuilding": {
      "title": "Team Building",
      "details": "<p class=\"text-justify\">Fusce consequat tellus pharetra congue mattis. Pellentesque iaculis arcu libero, vel volutpat neque ornare quis. Vivamus sodales, justo at laoreet sagittis, libero quam venenatis est, eu semper ligula urna sit amet nibh. Nulla id ipsum egestas, faucibus sem posuere, elementum neque. Nullam hendrerit ex vel sem consequat congue. Maecenas id ex rhoncus, varius nulla tempor, sollicitudin nulla. Donec posuere imperdiet libero eget varius. Donec ornare est quis est maximus mollis. Sed eget mauris nibh. Curabitur tempus sapien leo, in aliquam erat lacinia imperdiet. Etiam et rhoncus orci. Sed at convallis enim. Phasellus a justo nec ligula tempor aliquam in ut arcu. Etiam gravida, ante et dictum interdum, massa velit sodales lorem, aliquam vestibulum metus ipsum sit amet libero. Praesent faucibus tempus pulvinar. Nulla facilisi.</p>"
    },
    "freeneasy": {
      "title": "Free and Easy",
      "details": "<p class=\"text-justify\">Nam sollicitudin mollis arcu, sit amet viverra augue egestas in. Suspendisse suscipit ornare mollis. Aenean a blandit dui. Pellentesque erat enim, hendrerit sit amet tincidunt nec, placerat non quam. Integer in finibus tellus, non consectetur odio. Sed gravida leo felis, viverra congue tellus luctus suscipit. Aliquam sollicitudin leo at purus gravida, sit amet euismod est imperdiet. Nunc sit amet felis neque. Etiam urna nunc, cursus ac ligula sit amet, aliquet convallis lectus. Etiam non est est. Proin laoreet pharetra dui non varius. Cras et vulputate leo. Quisque et lectus ut velit laoreet commodo et at lorem.</p>"
    },
    "brandpresentation": {
      "title": "Brand Presentation",
      "details": "<p class=\"text-justify\">Donec tincidunt, ipsum nec convallis imperdiet, tortor massa semper nulla, at hendrerit sem diam nec leo. Nulla sit amet elit ac turpis vestibulum sodales nec quis nunc. Mauris malesuada enim vel nisl porttitor, vitae fermentum massa tempus. Curabitur lacinia hendrerit sem, at vestibulum arcu sagittis a. Aenean tincidunt nisl sed leo elementum, non cursus sem porttitor. Pellentesque ut augue in leo aliquam accumsan. Suspendisse quam dolor, cursus quis molestie ac, tempor sed augue. Maecenas pellentesque felis ipsum, et scelerisque arcu eleifend eget. Quisque fermentum ipsum neque, vel tincidunt odio sodales eget. Phasellus ullamcorper massa et tellus aliquam, eget suscipit neque iaculis. Integer rhoncus lorem a ante interdum bibendum. Vivamus non ligula dolor.</p>"
    },
    "mbc": {
      "title": "MBC",
      "details": "<p class=\"text-justify\">Aliquam fermentum scelerisque diam, ultricies accumsan metus molestie quis. Donec fermentum nec purus porttitor cursus. Donec eu luctus dui. Phasellus aliquam nisl ut convallis laoreet. Integer vestibulum mauris sit amet tortor interdum malesuada. Nullam quis ex non arcu auctor posuere. Vivamus a eros tincidunt, mollis risus a, lacinia elit. Nullam eu metus quis lectus facilisis auctor sodales vel sapien. Aenean lorem dui, rhoncus eget ullamcorper vitae, ultricies id ligula. Vestibulum aliquam faucibus eros, eu tincidunt enim ultricies sed.</p>"
    }
  }


  $(".navbar-nav .nav-link").on("click", function (e) {
    $(".navbar-nav").find(".active").removeClass("active");
    $(this).addClass("active");
    setData(data[$(this).data('key')]);
  });

  $('.navbar-nav a').on('click', function () {
    $('.navbar-toggler').click();
  });

  $('#btn-register').on('click', function (e) {
    e.preventDefault();
    registration();
  });

  function registration()
  {
    alert('goes to registration');
  }

  function setData(data)
  {
    $('#content .title').html('').html(data.title);
    $('#content .details').html('').html(data.details);  
  }
  
  setData(data.intro);
});

